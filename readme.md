# cue node server deployment ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This server hosts an api backend and 
* currently this project connects to Pushwoosh but its main goal is to have a generic interface that can connect to any push server api

##reminder
public folder is not pushed, it is the supposed location of production build of multiple angular project

## Installation

### Requirements
* node, npm

## Deployment
* dev - node server.js
* prod - deploy to SAP cloud foundry using cloud foundry CLI, cf push
