const express = require('express');
const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const api = require('./api/api');
const app = express();
const port = process.env.PORT || 3000;
const morgan = require('morgan');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
require('dotenv').config();

//middleware block
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}))//does not support rich text
app.use(bodyParser.json())//accepts json request
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin','*');//allow all access from any domain
    res.header('Access-control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS'){
        res.header('Acces-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({})
    }
    next();
})
//route logger
app.use(morgan('dev'));
//backend
app.use('/api',api);
//cue front end
// app.use(express.static(__dirname + '/public/oncue-web'));
// app.get('/oncue-web',(req,res)=>res.sendFile(path.join(__dirname, '/public/oncue-web/index.html')));
// app.use(express.static(__dirname + '/public/oncue-banner'));
// app.get('/oncue-banner',(req,res)=>res.sendFile(path.join(__dirname, '/public/oncue-banner/index.html')));
// app.use(express.static(__dirname + '/public/oncue-1dot6'));
// app.get('/oncue-1dot6',(req,res)=>res.sendFile(path.join(__dirname, '/public/oncue-1dot6/index.html')));

app.use(express.static(path.join(__dirname, '/public/oncue-web')));
app.use('/oncue-web',express.static(path.join(__dirname, '/public/oncue-web')));
app.use(express.static(path.join(__dirname, '/public/oncue-banner')));
app.use('/oncue-banner',express.static(path.join(__dirname, '/public/oncue-banner')));
app.use(express.static(path.join(__dirname, '/public/oncue-1dot6')));
app.use('/oncue-1dot6',express.static(path.join(__dirname, '/public/oncue-1dot6')));


//404 catch
app.use((req,res,next)=>{
    const error = new Error('not found');
    error.status = 404;
    next(error);
})
//error handler
app.use((error,req,res,next)=>{
    console.log("error handler");
    res.status(error.status || 500).send({"server runtime error":error.message});
})


//server instance
const server = http.createServer(app);
server.listen(port,()=>{
    console.log("..running..");
    console.log("environment variables:",process.env);
});