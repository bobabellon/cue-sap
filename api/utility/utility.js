const crypto = require('crypto');
const requestModule = require('request');

module.exports = {
    enc:function(input){
        const crypto = require('crypto');
        const algorithm = 'aes-192-cbc';
        const password = process.env.SECRET;
        // Use the async `crypto.scrypt()` instead.
        const key = crypto.scryptSync(password, 'salt', 24);
        // Use `crypto.randomBytes` to generate a random iv instead of the static iv
        // shown here.
        const iv = Buffer.alloc(16, 0); // Initialization vector.

        const cipher = crypto.createCipheriv(algorithm, key, iv);

        let encrypted = cipher.update(input, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        console.log(encrypted);
        return encrypted;
    },
    dec:function(input){
        const algorithm = 'aes-192-cbc';
        const password = process.env.SECRET;
        // Use the async `crypto.scrypt()` instead.
        const key = crypto.scryptSync(password, 'salt', 24);
        // The IV is usually passed along with the ciphertext.
        const iv = Buffer.alloc(16, 0); // Initialization vector.

        const decipher = crypto.createDecipheriv(algorithm, key, iv);

        // Encrypted using same algorithm, key and iv.
        const encrypted = input;
        let decrypted = decipher.update(encrypted, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        console.log(decrypted);
        return decrypted;
    },
    httpPost : function(url,body,res,isFormEncoded = false){
        console.log("url",url);
        return new Promise(function(resolve,reject){
            let reqObj = {
                url: url,
                headers: {
                    "Content-Type":`application/${isFormEncoded ? "x-www-form-urlencoded" : "json"}`
                },
                json: true
            }
            if(!isFormEncoded){
                reqObj["body"] = body;
            }
            requestModule.post(reqObj, function (error, response, body){
                if(error){
                    //console.log("error");
                    reject(error);
                }else{
                    console.log("not error", response.statusCode);
                    if(response.statusCode == 200 || response.statusCode == 201){
                        resolve(response.body);
                    }else{
                        reject(response.body);
                    }
                }
                console.log(response.body);
            });
        });
        
    },
    httpGet : function(url,body,res){
        //console.log("httpget");
        
        return new Promise(function(resolve,reject){
                console.log(url);
                requestModule.get({
                    url: `${url}${bodyToUrlQuery(body)}`,
                    headers: {
                        "Content-Type":"application/json"
                    },
                    json: true,
                }, function (error, response, body){
                    //console.log("call back of get http");
                    if(error){
                        //console.log("error");
                        reject(error);
                    }else{
                        //console.log("else, status:",response.statusCode);
                        if(response.statusCode == 200){
                            //console.log("resolve");
                            resolve(response.body);
                        }else{
                            console.log("reject");
                            reject(response.body);
                        }
                        console.log(response.body);
                    }
                });
            
        });
        
    },
    httpDelete : function(url,body,res,isFormEncoded = false){
        return new Promise(function(resolve,reject){
            let reqObj = {
                url: url,
                headers: {
                    "Content-Type":`application/${isFormEncoded ? "x-www-form-urlencoded" : "json"}`
                },
                json: true
            }
            if(!isFormEncoded){
                reqObj["body"] = body;
            }
            requestModule.delete(reqObj, function (error, response, body){
                if(error){
                    //console.log("error");
                    reject(error);
                }else{
                    //console.log("not error", response.statusCode);
                    if(response.statusCode == 200 || response.statusCode == 201){
                        resolve(response.body);
                    }else{
                        reject(response.body);
                    }
                }
                console.log(response.body);
            });
        });
        
    },
    httpPut : function(url,body,res,isFormEncoded = false){
        return new Promise(function(resolve,reject){
            let reqObj = {
                url: url,
                headers: {
                    "Content-Type":`application/${isFormEncoded ? "x-www-form-urlencoded" : "json"}`
                },
                json: true
            }
            if(!isFormEncoded){
                reqObj["body"] = body;
            }
            requestModule.put(reqObj, function (error, response, body){
                if(error){
                    //console.log("error");
                    reject(error);
                }else{
                    //console.log("not error", response.statusCode);
                    if(response.statusCode == 200 || response.statusCode == 201){
                        resolve(response.body);
                    }else{
                        reject(response.body);
                    }
                }
                console.log(response.body);
            });
        });
        
    },
    createLoginResponse:function(jwt,user,access_token){
        console.log("jwt",jwt);
        console.log("user",user);
        return {
            token: jwt,
            name: user.name,
            uid:user.uid,
            firstName:user.firstName,
            lastName:user.lastName,
            access_token:access_token
          };
    },
    createKeywordResponse:function(result){
        return {
            extractedKeywordEntry: {
                keyword:result.extractedKeywordEntry.keyword,
                numberOfResults:result.extractedKeywordEntry.numberOfResults,
                pageUrl:"/api/products",
            },
            responseId:result.responseId

        }
    },
    createCartResponse:function(result){
        return{
            carts:[
                
            ]
        }
    },
    createQueryString:function(object){
        var query = "";
    var counter = 1;
    var length = Object.keys(object).length;
    if(length > 0) query += '?';
    for(let key in object){
        query = query.concat(key).concat(`=${object[key]}`);
        if(counter < length){
            query = query.concat("&");
        }
        counter++;
    }
    //console.log("get url:",query);
    return query;
    }
}

function bodyToUrlQuery(object){
    //console.log("body to url query");
    var query = "";
    var counter = 1;
    var length = Object.keys(object).length;
    if(length > 0) query += '?';
    for(let key in object){
        query = query.concat(key).concat(`=${object[key]}`);
        if(counter < length){
            query = query.concat("&");
        }
        counter++;
    }
    //console.log("get url:",query);
    return query;
}