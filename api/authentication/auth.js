
const jwt = require('jsonwebtoken');
const utility = require('../utility/utility');
const _ = require('lodash');

module.exports = {
    checkAuth: function(req,res,next){
        try{
            //console.log(req.headers)
            let authorization = req.get('Authorization');
            let authorizationSplit = authorization.split(" ",2);
            console.log(authorization);
            if(authorizationSplit.length > 1){
                let decoded = jwt.verify(authorizationSplit[1],process.env.SECRET);
                //insert service to check if token is still valid, use refresh token if not
                req.userData = decoded;
            }else{
                throw "Auth failed"
            }
            
        }catch(err){
            console.log(err);
            return res.status(401).json({message:"Auth failed"})
        }
        next();
    }
}