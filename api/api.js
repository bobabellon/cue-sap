const express = require('express');
const router = express.Router();
const _ = require('lodash');
const app = express();

const login = require('./routes/login');
app.use('/login', login);
const home = require('./routes/home');
app.use('/home', home);
const keyword = require('./routes/keyword');
app.use('/keyword', keyword);
const notifications = require('./routes/notifications');
app.use('/notifications',notifications);
const products = require('./routes/products');
app.use('/products',products);
const projects = require('./routes/projects');
app.use('/projects',projects);
const cart = require('./routes/cart');
app.use('/cart',cart);
const orders = require('./routes/orders')
app.use('/orders',orders);
const test = require('./routes/test')
app.use('/test',test);
module.exports = app;
//asdas