
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const utility = require('../utility/utility');

router.post('/',(req,res,next) =>{
    console.log(req.body);
    var tokenObject = {};
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/authorizationserver/oauth/token?client_id=trusted_client&client_secret=secret&grant_type=password&username=${req.body.username}&password=${req.body.password}`,{},res)
    .then((token)=>{
        console.log("tokenObject", token);
        tokenObject = token;
        return utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.body.username}?access_token=${tokenObject.access_token}`,{},res)
    }).then((user)=>{
        console.log(user);
        console.log(tokenObject);
        tokenObject.userId = user.uid;
        var jwtToken = jwt.sign(tokenObject,process.env.SECRET);//can add options to set expiration
        console.log(JSON.stringify(user));
        res.status(200).json(utility.createLoginResponse(jwtToken,user,tokenObject.access_token));
    }).catch((error)=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

module.exports = router;