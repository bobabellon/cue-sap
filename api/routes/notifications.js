const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');

router.get('/', auth.checkAuth, (req,res,next) =>{
    console.log("user",req.userData);
    utility.httpPost(`${process.env.FIREBASE_SERVICE}/getPullNotification`,{userId:req.userData.userId},res)
    .then((notifications)=>{
        res.status(200).json(notifications);
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
    
});

module.exports = router;