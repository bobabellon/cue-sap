const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');

router.post('/', (req,res,next) =>{
    console.log(req.query);
    res.status(200).json({message:req.query});
});

router.get('/', (req,res,next) =>{
    console.log(req.query);
    res.status(200).json({message:req.query});
});

router.post('/send',(req,res,next) =>{
    console.log(req.query);
    utility.httpPost(`http://localhost:3000/api/test?code=${req.query.code}&qty=${req.query.qty}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
        
    })
    
});

module.exports = router;