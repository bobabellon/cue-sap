const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');

router.get('/',auth.checkAuth, (req,res,next) =>{
    console.log("access token", req.userData.access_token);
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/orders?pageSize=10&currentPage=0&access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        if(result.pagination.totalResults == 0){
            result.orders = [];
        }
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.post('/',auth.checkAuth, (req,res,next) =>{
    console.log("access token", req.userData.access_token);
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/orders?cartId=${req.body.cartId}&access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        if(result.pagination.totalResults == 0){
            result.orders = [];
        }
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

module.exports = router;