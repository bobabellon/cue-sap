const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');
const querystring = require('query-string');

router.get('/',auth.checkAuth, (req,res,next) =>{
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        console.log(result);
        var promises = [];
        if(!_.isNil(result.carts[0])){
            if(result.carts[0].totalItems > 0){
                for(let index =0; index < result.carts[0].entries.length; index++ ){
                //console.log(result.carts[0].entries[index]);
                promises.push(utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/${result.carts[0].entries[index].product.code}?fields=images`,{},res));
                }
                var cart = result;
                Promise.all(promises).then((result)=>{
                    console.log("image list", result);
                    for(let index = 0; index < cart.carts[0].entries.length; index++){
                        console.log("promis push " + index + " :" + result[index].images[0].url);
                        cart.carts[0].entries[index].product.image = `${process.env.ASSETS_URL}${result[index].images[0].url}` //get first image only
                    }
                    res.status(200).json(cart);
                });
            }else{
                result.carts[0].entries = [];
                res.status(200).json(result);     
            }
        }else{
            res.status(200).json(result);  
        }
        
        //res.status(200).json(result);
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});
router.post('/entry',auth.checkAuth, (req,res,next) =>{//add items to cart
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("req.body",req.body);
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.body.cartId}/entries?code=${req.body.productCode}&qty=${req.body.quantity}&access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
        
    })
});
router.put('/entry',auth.checkAuth, (req,res,next) =>{//update quantity of entry
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("req.body",req.body);
    utility.httpPut(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.body.cartId}/entries/${req.body.entryCode}?qty=${req.body.quantity}&access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
        
    })
});
router.delete('/entry',auth.checkAuth, (req,res,next) =>{//delete items from cart
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpDelete(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.body.cartId}/entries/${req.body.entryCode}?access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json({message:"successfully deleted cart entry"});
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.post('/create',auth.checkAuth, (req,res,next) =>{//delete items from cart
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpDelete(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/?access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json({message:"successfully deleted cart entry"});
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.get('/delivery',auth.checkAuth, (req,res,next) =>{ //get all delivery address of cartId
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.query.cartId}/deliverymodes?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result);
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.put('/delivery',auth.checkAuth, (req,res,next) =>{ //set delivery address of cartId
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpPut(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.query.cartId}/addresses/delivery?addressId=${req.body.addressId}&access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json({message:"delivery address has been set"});
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.put('/delivery-address',auth.checkAuth, (req,res,next) =>{
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("req.body",req.body);
    utility.httpPut(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.body.cartId}/deliverymode?deliveryModeId=${req.body.deliveryModeId}&access_token=${req.userData.access_token}`,{},res,true)
    .then((result)=>{
        res.status(200).json({message:"delivery mode has been set"});
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.post('/address',auth.checkAuth, (req,res,next) =>{
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    req.body.access_token = req.userData.access_token;
    var query = utility.createQueryString(req.body);
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/addresses${query}`,{},res,true)
    .then((result)=>{
        res.status(200).json(result);
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.post('/payment-details',auth.checkAuth, (req,res,next) =>{
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    req.body.access_token = req.userData.access_token;
    var query = querystring.stringify(req.body);
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/carts/${req.query.cartId}/paymentdetails?${query}`,{},res,true)
    .then((result)=>{
        res.status(200).json(result);
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

module.exports = router;
