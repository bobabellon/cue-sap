
const express = require('express');
const router = express.Router();

router.get('/',(req,res,next) =>{
    res.status(200).json(require('../../config/home-details'));
});

module.exports = router;