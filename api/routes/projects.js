const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');

router.get('/',auth.checkAuth, (req,res,next) =>{//get all projects
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});
router.put('/',auth.checkAuth, (req,res,next) =>{//create project
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});
router.delete('/',auth.checkAuth, (req,res,next) =>{//delete project
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("project id", req.body.projectId);
    utility.httpDelete(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects/${req.body.projectId}?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});
router.get('/detail',auth.checkAuth, (req,res,next) =>{//get project details
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("project id", req.query.project_id);
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects/${req.query.project_id}?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.put('/product',auth.checkAuth, (req,res,next) =>{//add product to project
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("product code", req.body.productCode);
    utility.httpPut(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects/${req.body.productCode}?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

router.delete('/product',auth.checkAuth, (req,res,next) =>{//delete project items
    console.log("userId",req.userData.userId);
    console.log("access token", req.userData.access_token);
    console.log("project id", req.body.projectId);
    console.log("product code", req.body.productCode)
    utility.httpDelete(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/users/${req.userData.userId}/customerprojects/${req.body.projectId}/products/${req.body.productCode}?access_token=${req.userData.access_token}`,{},res)
    .then((result)=>{
        res.status(200).json(result)
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

module.exports = router;