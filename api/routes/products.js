const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');
const auth = require('../authentication/auth');

router.get('/', (req,res,next) =>{
    console.log("req.query",req.query);
    var productList = [];
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/search?query=${req.query.keyword}`,{},res)
    .then((searchResult)=>{
        //console.log("product result", searchResult.products);
        productList = searchResult.products;
        //filter out product list;
        let filter1  = productList.filter(product => {
            if(_.isNil(product.name)) return false;
            else return product.name.toLowerCase().includes(req.query.keyword.toLowerCase().trim());
        });
        let filter2 = productList.filter(product => {
            if(!product.description) return true //not all results have descriptions
            else return product.description.toLowerCase().includes(req.query.keyword.toLowerCase().trim());
        });
        console.log("filter1", filter1);
        console.log("filter2",filter2);
        productList = _.uniqBy(_.concat(filter1,filter2),'code');        

        var promises = [];
        for(let index = 0; index < productList.length; index++){
            //remove/edit unnecessary fields
            delete productList[index].manufacturer;
            delete productList[index].multidimensional;
            delete productList[index].priceRange;
            productList[index].price = productList[index].price.value
            //query array for immages
            //console.log("product code", productList[index].code);
            promises.push(utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/${productList[index].code}?fields=images`,{},res));
        }
        Promise.all(promises).then((result)=>{
            //console.log("image list", result);
            for(let index = 0; index < productList.length; index++){
                //console.log(result[0].images[0].url);
                productList[index].image = result[index].images[0].url //get first image only
            }
            res.status(200).json(productList);
        });
        
    }).catch((error)=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
    
});

router.get('/detail',(req,res,next) =>{
    var product = {};
    utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/${req.query.productCode}`,{},res)
    .then((productResult)=>{
        product = productResult;
        //remove/edit unnecessary fields
        delete product.manufacturer;
        delete product.multidimensional;
        delete product.priceRange;
        product.price = product.price.value
        return utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/${product.code}?fields=images`,{},res)
    })
    .then((productImages)=>{
        console.log(productImages);
        product.image = productImages.images[0].url
        res.status(200).json(product);
    })
    .catch((error)=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
        //res.status(500).json({error:error})
    })
});

module.exports = router;