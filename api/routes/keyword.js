const express = require('express');
const router = express.Router();
const _ = require('lodash');
const utility = require('../utility/utility');

router.post('/',(req,res,next) =>{
    console.log("req.query",req.query);
    var keywordResponse;
    utility.httpPost(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/extractKeyword`,req.body,res)
    .then((result)=>{
        keywordResponse = utility.createKeywordResponse(result);
        //call search query to modify reponseId of keywordResponse if needed
        //a middleware in api/products is filtering further the search response
        console.log("keywordresponse keyword" + keywordResponse.extractedKeywordEntry.keyword);
        return utility.httpGet(`${process.env.HYBRIS_SERVICE}/rest/v2/powertools/products/search?query=${keywordResponse.extractedKeywordEntry.keyword}`,{},res);
    }).then((searchResult)=>{
        console.log("product result", searchResult.products);
        let productList = searchResult.products;
        //filter out product list;
        let filter1  = productList.filter(product => {
            if(_.isNil(product.name)) return false;
            else return product.name.toLowerCase().includes(keywordResponse.extractedKeywordEntry.keyword.toLowerCase().trim());
        });
        let filter2 = productList.filter(product => {
            if(!product.description) return true //not all results have descriptions
            else return product.description.toLowerCase().includes(keywordResponse.extractedKeywordEntry.keyword.toLowerCase().trim());
        });
        console.log("filter1", filter1);
        console.log("filter2",filter2);
        productList = _.uniqBy(_.concat(filter1,filter2),'code');        
        if(productList.length > 0) keywordResponse.responseId = 1;
        else keywordResponse.responseId = 0;
        keywordResponse.extractedKeywordEntry.numberOfResults = productList.length;
        res.status(200).json(keywordResponse);
        
    }).catch(error=>{
        console.log("router catch error", error);
        let err = new Error(JSON.stringify(error));
        err.status = 500;
        next(err);
    });

    
});



module.exports = router;